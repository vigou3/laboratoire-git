<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Gestion de versions avec Git

[*Gestion de versions avec Git*](https://gitlab.com/vigou3/gestion-versions-avec-git/-/releases/) est un laboratoire (ou atelier) d'introduction au système de gestion de versions Git offert dans le cadre du cours [IFT-1902 Programmation avec R pour l'analyse de données](https://www.ulaval.ca/les-etudes/cours/repertoire/detailsCours/ift-1902-informatique-pour-actuaires.html) à l'[École d'actuariat](https://www.act.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

Outil essentiel en contexte de travail collaboratif, le système de gestion de versions permet de régler les problèmes de suivi de la plus récente version d'un fichier, de partage de fichiers entre les membres d'une équipe et de mise en commun des contributions. Comme les versions successives sont entreposées dans un référentiel situé sur un serveur central, le système fournit également une forme de copie de sauvegarde du code source d'un projet. 

Développé à l'origine par Linus Torvalds pour administrer le code source du noyau du système d'exploitation Linux, [Git](https://git-scm.com) est aujourd'hui le système de gestion de versions le plus utilisé dans le monde.

Le laboratoire consiste en une présentation entrecoupée d'exemples et d'exercices.

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

«Gestion de versions avec Git» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.

## Modèle de développement

Le processus de rédaction et de maintenance du projet suit le modèle [*Gitflow Workflow*](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow ). Seule particularité: la branche *master* se trouve dans le dépôt [`gestion-versions-avec-git`](https://gitlab.com/vigou3/gestion-versions-avec-git) dans GitLab, alors que la branche de développement se trouve dans le dépôt [`gestion-versions-avec-git-devel`](https://projets.fsg.ulaval.ca/git/scm/vg/gestion-versions-avec-git-devel) dans le serveur BitBucket de la Faculté des sciences et de génie de l'Université Laval.

Prière de passer par le dépôt `gestion-versions-avec-git-devel` pour proposer des modifications; consulter le fichier `CONTRIBUTING.md` pour la marche à suivre.
