===
=== Gestion de versions avec Git
===

# 2025.02 (2025-02-12)

## Nouveautés

- Exercice sur la création de branches: étape additionnelle de
  suppression d'une fichier dans une branche secondaire afin
  d'illustrer le fonctionnement  de la commande `git rm`.
- Section «Branches»: diapositive pour expliquer que `git rm` indexe
  automatiquement la suppression des fichiers.

## Changements

- L'adjectif «externe» accolé à «référentiel» a été supprimé.


# 2024.10a (2024-10-06)

Plusieurs modifications ont trait à la branche principale, son
traitement dans la version précédente pouvant causer de la confusion.

## Nouveautés

- Section «Branches»: diapositive consacrée au nom de la branche
  principale.
- Section «Branches»: exercice sur la vérification du nom des branches
  locales et distantes.
- Exercice sur `git commit`: informations additionnelles sur la
  manière de terminer l'édition d'un message de validation, sous
  Windows et sous macOS. Ajout motivé principalement par les nouvelles
  fonctionnalités du Bloc-notes introduites dans Windows 11.
- Section «Trucs et astuces»: procédure pour déplacer un dépôt vers un
  autre référentiel.

## Changements

- Introduction: la diapositive sur la configuration de la variable
  `init.defaultBranch` est supprimée.
- Exercice sur `git clone`: l'étape d'activation d'une branche
  `master` est supprimée.
- Section «Branches»: l'avertissement sur le changement de branche
  sans y avoir effectué au moins une archive est déplacé après la
  présentation de la commande `git switch`.
- Section «Aller plus loin»: ordre des diapositives modifié.
- Diapositive «Git et RStudio»: ajout d'une mention que l'utilisation
  des projets RStudio n'est pas vraiment recommandée.


# 2024.10 (2024-09-30)

## Changements

- Exercice sur `git pull`: l'étape 4 a été séparée en deux.
- Section «Branches»: l'avertissement sur le basculement vers une
  autre branche sans y avoir effectué au moins une archive a été
  retravaillé. Son niveau d'importance a été réduit et le message est
  davantage générique (puisqu'il ne concerne pas uniquement la branche
  principale). Une astuce a aussi été ajoutée à la diapositive pour
  expliquer comment récupérer la branche disparue.


# 2024.08 (2024-08-27)

## Changements

- Section «Bases de Git»: exercice sur `git push` et `git pull`
  complètement révisé pour simuler la collaboration en créant une
  seconde copie du dépôt sur son propre poste de travail. Cela permet
  d'éviter la modification d'un fichier via l'interface web du
  référentiel, qui n'est pas vraiment une bonne habitude à prendre.
- Section «Branches»: l'exemple avancé est maintenant celui du présent
  projet (ce qui ne change autrement rien à la diapositive).
- Image de la couverture retouchée.


# 2024.01 (2024-01-07)

## Nouveautés

- Section «Démarrer avec Git»: ajout d'une analogie entre le cycle de
  travail Git et un assistant personnel de suivi d'un projet.
- Diapositive «Cycle de travail» entièrement remaniée pour en
  simplifier la lecture, l'arrimer à l'analogie du cycle de travail et
  mieux faire ressortir le rôle de l'index.
- Diapositive «Interface»: ajout d'une phrase pour préciser que
  l'interface est la ligne de commande pour la formation.
- Ajout dans le projet de diapositives pour la réalisation d'une vidéo
  d'introduction à l'aide d'un tableau lumineux.

## Changements

- Diapositive «Vous avez vécu cette situation»: images de Freepik
  remplacées par des captures d'écran du film Carnival of Souls (afin
  de respecter la licence d'utilisation).
- Diapositive «Git» (présentation générale): ajout d'un logo de Git
  (et d'une icône de guitare à côté de la prononciation «guitte»).
- Section «Démarrer avec Git» renommée «Concepts fondamentaux». Début
  déplacé après les diapositives sur les préparatifs et la
  configuration.
- Diapositives «Deux types de fichiers» et «Concept fondamental»
  fusionnées en une diapositive «Statut des fichiers» placée après
  l'illustration du cycle de travail.
- Diapositive «Git vous parle, écoutez» déplacée de la section
  «Démarrer avec Git» à la section «Bases de Git».
- Les diapositives sur les commandes `git clone`, `git add`, `git
  commit`, `git push` et `git pull` utilisent des nouvelles
  illustrations dérivées de la nouvelle illustration du cycle de
  travail.
- Diapositive «Travailler sur le projet» supprimée. Son contenu est
  partiellement intégré à la diapositive «Vérifier l'état des
  fichiers».
- Diapositive «Cycle de travail dans une branche»: utilisation d'une
  partie de la nouvelle illustration du cycle de travail.


# 2023.02 (2023-02-03)

Le référentiel BitBucket de la Faculté des sciences et de génie de
l'Université Laval n'accepte que `master` comme nom de branche par
défaut. Pour éviter des problèmes en aval chez les étudiants, la
formation revient à `master` comme branche par défaut plutôt que
`main`.

## Changements

- Nouvelle diapositive de configuration pour utiliser `master` comme
  nom de branche par défaut.
- Ajout de la commande `git switch -c master` dans l'exercice de
  clonage du dépôt.
- Utilisation partout de `master` plutôt que `main` comme nom de
  branche principale.


# 2023.01 (2023-01-04)

## Nouveautés

- Diapositive «Préalable»: ajout dans la note que GitHub exige
  maintenant ses propres outils pour l'authentification, ce qui
  justifie indirectement que le document utilise GitLab dans les
  exemples.
- Section «Bases de Git»: ajout de notes que c'est aux étapes `git
  clone` et `git push` que l'authentification au serveur est
  nécessaire selon que le dépôt est privé ou public, dans l'ordre.

## Changements

- Diapositive «Git et RStudio»: URL mise à jour, la compagnie RStudio
  ayant changé de nom pour Posit en novembre 2022.


# 2022.10 (2022-10-07)

## Nouveautés

- Section «Branches»: note d'information sur l'ajout récent de la
  commande `switch` et sur le fait que c'était avant cela la commande
  `checkout` qui jouait le même rôle de création de branches et de
  basculement de l'une à l'autre.

## Changements

- Diapositive «Créer des branches et basculer de l’une à l’autre»:
  l'ordre de présentation des deux commandes a été inversé sur la
  diapositive pour correspondre au titre, donc `git switch -c` en
  premier, puis `git switch`.
- Un identifiant de branche obsolète dans une figure suite aux
  changement effectués dans la version 2022.02 a été corrigé.


# 2022.06 (2022-06-23)

## Changements

- Modification du nom du projet.
- L'avertissement de ne pas basculer vers une autre branche depuis
  `main` sans y avoir d'abord effectué au moins une archive est de
  retour. L'expérience dicte qu'il est toujours nécessaire.
- Diapositives «Modifier la plus récente archive» et «Annuler la plus
  récente archive»: remplacer `HEAD` par `HEAD^`.


# 2022.02 (2022-02-04)

## Nouveautés

- Prélable à la formation: disposer d'un compte dans un site
  d'hébergement Git gratuit (GitLab., GitHub, BitBucket, ...).
- Exercices maintenant basés sur un projet `formation-test` créé dans
  le site d'hébergement.
- «Solutions» des exercices fournies avec les énoncés afin de
  faciliter l'apprentissage autonome.
- Exemple avancé de branches: mention qu'il s'agit du modèle «Gitflow
  Workflow».
- Diapositives dans la section «Aller plus loin» sur des problèmes
  fréquemment rencontrés: «Modifier la plus récente archive», «Annuler
  la plus récente archive» et «Déplacer une archive vers une autre
  branche», de même qu'un avertissement.

## Changements

- Diapositive «Préparatifs» simplifiée et déplacée au début de la
  section «Démarrer avec Git».
- Branche principale nommée *main* par défaut et *master* comme
  alternative, plutôt que l'inverse.
- L'avertissement de ne pas basculer vers une autre branche depuis
  `main` sans y avoir d'abord effectué au moins une archive a été
  supprimé. Il ne devrait plus être nécessaire avec les nouveaux
  exercices.
- Suppression de la couverture arrière.


# 2021.05 (2021-05-18)

## Nouveauté majeure

Le projet «Ligne de commande et gestion de versions avec Git» est
scindé en deux laboratoires: «Ligne de commande Unix» et «Gestion de
versions avec Git». J'ai conservé ci-dessous (et dans le dépôt Git)
l'historique du projet unique.

## Changements

- Utilisation de «publier» plutôt que «pousser» pour expliquer `git
  push`.
- Mention (seulement) que la branche principale peut maintenant
  aussi se nommer `main`.
- Correction à l'exemple élaboré: la branche de développement de
  «Programmer avec R» se nomme maintenant `devel`.


# 2021.02 (2021-02-11)

## Nouveautés

- Introduction à Git: ajout d'images dans la diapositive «Vous avez
  vécu cette situation».

## Changements

- Branches Git: branche `release` dans l'exemple d'organisation avancé
  renommée `releases`.


# 2021.01 (2021-01-19)

## Nouveautés

- Ligne de commande: présentation de la commande `cp`.
- Ligne de commande: exercices sur la copie et la suppression de
  fichiers.
- Transfert de données et redirection: diapositive sur la philosophie
  Unix.
- Démarrer avec Git: ajout d'une mention qu'un fichier considéré
  «modifié» par Git peut soit avoir changé, soit avoir été ajouté sous
  suivi, soit avoir été supprimé. Ce dernier type de modification
  était mal compris.
- Bases de Git: ajout d'un «exercice» de préparatifs pour bien
  identifier, à la ligne de commande, le répertoire dans lequel la
  dépôt sera cloné afin de réduire une source fréquente de confusion.

## Changements

- Commandes essentielles: suppression de la présentation de la
  commande `touch`. Causait plus de confusion qu'autre chose.
- Commandes essentielles: section scindée en deux: «Navigation dans
  le système de fichiers» et «Gestion des fichiers».
- Diapositive «Rudiments de la ligne de commande» déplacée vers
  l'introduction.
- Diapositive sur les raccourcis clavier utiles déplacée vers
  l'introduction.


# 2020.08 (2020-08-31)

## Nouveautés

- Diapositive sur les raccourcis clavier utiles à la ligne de commande.
- Invite de commande colorée dans les exemples.


# 2020.02 (2020-02-12)

## Nouveautés

- Partie sur Git, section Aller plus loin: ajout d'une diapositive sur
  l'utilisation de Git dans RStudio.
  

# 2020.01 (2020-01-07)

## Nouveautés

- Représentation graphique du flux des données à la ligne de commande
  Unix.

## Changements

- Terme «archiver» utilisé comme équivalent français de «commit» tel
  que recommandé par l'Office québécois de la langue française.


# 2019.11 (2019-11-08)

## Nouveautés

- Diapositive sur quelques «Unix-ismes» importants pour utiliser la
  ligne de commande.
- Section sur l'exécution de scripts. Le matériel contient maintenant
  un petit fichier de script `bonjour.sh` pour les fins de l'exercice
  sur ce sujet.

## Corrections

- Dans l'explication de la commande de `git push -u`, l'argument
  `<branche>` est le nom de la branche à relier au serveur sur une
  branche du même nom, et non pas le nom de la branche sur le serveur
  à relier à la branche courante.
  

# 2019.10-3 (2019-10-07)

## Nouveautés

- Indication à l'effet qu'il faut quitter son éditeur pour terminer
  l'édition du message de validation de l'étape `git commit`.
- Avis de ne pas basculer vers une nouvelle branche sans avoir au
  préalable effectué au moins une publication dans `master`.
- Diapositive dans la section «Aller plus loin» (avec Git) indiquant
  où sont enregistrés les mots de passe de Git sous Windows et sous
  macOS. Cela devrait aider les personnes qui ont fait une erreur lors
  de l'entrée de leur mot de passe et qui souhaitent le réinitialiser.

## Changements

- Un des exercices sur la redirection et le transfert de données (une
  alternative de l'utilisation de `tr`) remplacé par la redirection de
  `ls` vers un fichier.
- Les noms fictifs d'arguments ont été changés de `<foo>` ou `<origin>`
  pour `<branche>` ou `<serveur>`, par exemple. Cette nomenclature est
  probablement plus facile à comprendre pour les novices.
- L'exercice sur les branches a été modifié légèrement pour faire
  ajouter un fichier dans la branche `foo` plutôt que simplement
  modifier un fichier existant. Cela permet de voir plus clairement
  encore qu'un fichier ajouté dans `foo` ne se trouve pas dans
  `master`. De plus, la dernière partie de l'exercice consiste
  maintenant à supprimer les nouvelles branches.
- Dans l'illustration de l'exemple d'organisation locale de
  «Programmer avec R», les serveurs sont maintenant identifiés par
  leurs étiquettes `origin` et `projets` plutôt que par les noms
  GitLab et BitBucket. Les icônes permettent toujours d'identifier le
  type de serveur utilisé.


# 2019.10-2 (2019-10-03)

## Changements

- La vidéo sur le travail collaboratif avec Git ayant changé, l'url a
  été modifiée pour mener vers la nouvelle version.


# 2019.10-1 (2019-10-01)

## Nouveautés

- Strip XKCD pour la partie sur la ligne de commande. Parce
  qu'autrement, ce n'était pas juste.
- Diapositive «Git vous parle, écoutez».

## Changements

- La configuration de `core.editor` sous macOS est `open -W -n` plutôt
  que simplement `open`.
- Uniformisation du vocabulaire: deux instances de «référentiel»
  remplacées par «dépôt».


# 2019.10 (2019-09-29)

Version initiale.