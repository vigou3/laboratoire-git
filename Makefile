### -*-Makefile-*- pour préparer "Gestion de versions avec Git"
##
## Copyright (C) 2019-2022 Vincent Goulet
##
## 'make pdf' compile le document maitre avec XeLaTeX.
##
## 'make contrib' crée le fichier COLLABORATEURS.
##
## 'make update-copyright' met à jour l'année de copyright dans toutes
## les sources du document
##
## 'make zip' crée la distribution du matériel pédagogique.
##
## 'make release' téléverse la distribution dans GitLab, crée une
## nouvelle version et modifie les liens de la page web.
##
## 'make check-url' vérifie la validité de toutes les url présentes
## dans les sources du document.
##
## 'make all' est équivalent à 'make html' question d'éviter les
## publications accidentelles.
##
## Auteur: Vincent Goulet
##
## Ce fichier fait partie du projet "Gestion de versions avec Git"
## https://gitlab.com/vigou3/gestion-versions-avec-git


## Principaux fichiers
MASTER = gestion-versions-avec-git.pdf
ARCHIVE = ${MASTER:.pdf=.zip}
README = README.md
NEWS = NEWS
COLLABORATEURS = COLLABORATEURS
LICENSE = LICENSE

## Autres fichiers à inclure dans l'archive
OTHER = CONTRIBUTING.md

## Le document maitre dépend de tous les fichiers .tex autres que
## lui-même.
TEXFILES = $(addsuffix .tex, \
                       $(filter-out $(basename ${MASTER}),\
                                    $(basename $(wildcard *.tex))))

## Informations de publication extraites du fichier maitre
TITLE = $(shell grep " \\\\title" ${MASTER:.pdf=.tex} \
	| cut -d { -f 2 | tr -d })
REPOSURL = $(shell grep "newcommand{\\\\reposurl" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
YEAR = $(shell grep "newcommand{\\\\year" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
MONTH = $(shell grep "newcommand{\\\\month" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
VERSION = ${YEAR}.${MONTH}

## Auteurs à exclure du fichier COLLABORATEURS (regex)
OMITAUTHORS = Vincent Goulet|Inconnu|unknown

## Outils de travail
TEXI2DVI = LATEX=xelatex TEXINDY=makeindex texi2dvi -b
CP = cp -p
RM = rm -rf

## Dossier temporaire pour construire l'archive
BUILDDIR = builddir

## Dépôt GitLab et authentification
REPOSNAME = $(shell basename ${REPOSURL})
BASEAPI = https://gitlab.com/api/v4
APIURL = ${BASEAPI}/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Variables automatiques
TAGNAME = v${VERSION}


all: pdf

FORCE:

${MASTER}: ${MASTER:.pdf=.tex} ${TEXFILES} $(wildcard images/*)
	${TEXI2DVI} ${MASTER:.pdf=.tex}

${COLLABORATEURS}: FORCE
	git log --pretty="%an%n" | sort | uniq | \
	  grep -v -E "${OMITAUTHORS}" | \
	  awk 'BEGIN { print "Les personnes dont le nom [1] apparait ci-dessous ont contribué à\nl'\''amélioration de «${TITLE}»." } \
	       { print $$0 } \
	       END { print "\n[1] Noms tels qu'\''ils figurent dans le journal du dépôt Git\n    ${REPOSURL}" }' > ${COLLABORATEURS}

.PHONY: pdf
pdf: ${MASTER}

.PHONY: contrib
contrib: ${COLLABORATEURS}

.PHONY: release
release: update-copyright zip check-status create-release upload create-link publish

.PHONY: update-copyright
update-copyright: ${MASTER:.pdf=.tex} ${TEXFILES} ${RFILES}
	for f in $?; \
	    do sed -E '/^(#|%)* +Copyright \(C\)/s/-20[0-9]{2}/-$(shell date "+%Y")/' \
	           $$f > $$f.tmp && \
	           ${CP} $$f.tmp $$f && \
	           ${RM} $$f.tmp; \
	done

.PHONY: zip
zip: ${MASTER} ${README} ${NEWS} ${LICENSE} ${COLLABORATEURS} ${OTHER}
	if [ -d ${BUILDDIR} ]; then ${RM} ${BUILDDIR}; fi
	mkdir ${BUILDDIR}
	touch ${BUILDDIR}/${README} && \
	  awk 'state==0 && /^# / { state=1 }; \
	       /^## Auteur/ { printf("## Édition\n\n%s\n\n", "${VERSION}") } \
	       state' ${README} >> ${BUILDDIR}/${README}
	${CP} ${MASTER} ${NEWS} ${LICENSE} \
	      ${COLLABORATEURS} ${OTHER} \
	      ${BUILDDIR}
	cd ${BUILDDIR} && zip --filesync -r ../${ARCHIVE} *
	if [ -e ${COLLABORATEURS} ]; then ${RM} ${COLLABORATEURS}; fi
	${RM} ${BUILDDIR} 

.PHONY: check-status
check-status:
	@{ \
	    printf "%s" "vérification de l'état du dépôt local... "; \
	    branch=$$(git branch --list | grep ^* | cut -d " " -f 2-); \
	    if [ "$${branch}" != "master"  ] && [ "$${branch}" != "main" ]; \
	    then \
	        printf "\n%s\n" "! pas sur la branche main"; exit 2; \
	    fi; \
	    if [ -n "$$(git status --porcelain | grep -v '^??')" ]; \
	    then \
	        printf "\n%s\n" "! changements non archivés dans le dépôt"; exit 2; \
	    fi; \
	    if [ -n "$$(git log origin/master..HEAD | head -n1)" ]; \
	    then \
	        printf "\n%s\n" "changements non publiés dans le dépôt; publication dans origin"; \
	        git push; \
	    else \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: create-release
create-release:
	@{ \
	    printf "%s" "vérification que la version existe déjà... "; \
	    http_code=$$(curl -I ${APIURL}/releases/${TAGNAME} 2>/dev/null \
	                     | head -n1 | cut -d " " -f2) ; \
	    if [ "$${http_code}" = "200" ]; \
	    then \
	        printf "%s\n" "oui"; \
	        printf "%s\n" "-> utilisation de la version actuelle"; \
	    else \
	        printf "%s\n" "non"; \
	        printf "%s" "création d'une version dans GitLab... "; \
	        name=$$(awk '/^# / { sub(/# +/, "", $$0); print "Édition", $$0; exit }' ${NEWS}); \
	        desc=$$(awk ' \
	                      /^$$/ { next } \
	                      (state == 0) && /^# / { state = 1; next } \
	                      (state == 1) && /^# / { exit } \
	                      (state == 1) { print } \
	                    ' ${NEWS}); \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --output /dev/null --silent \
	             "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master" && \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --data tag_name="${TAGNAME}" \
	             --data name="$${name}" \
	             --data description="$${desc}" \
	             --output /dev/null --silent \
	             ${APIURL}/releases; \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: upload
upload:
	@printf "%s" "vérification que le registre est disponible... "
	@if curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --silent "${APIURL}/packages" \
	      | grep -q -E '"version":"${VERSION}"'; \
	then \
	    printf "%s\n" "ERREUR"; \
	    printf "%s\n" "! un registre existe déjà pour la version ${VERSION}"; \
	    false; \
	else \
	    printf "%s\n" "ok"; \
	fi
	@printf "%s\n" "téléversement de l'archive vers le registre..."; \
	curl --upload-file "${ARCHIVE}" \
	      --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	      --silent \
	      "${APIURL}/packages/generic/${REPOSNAME}/${VERSION}/${ARCHIVE}"
	@printf "%s\n" "ok"

.PHONY: create-link
create-link:
	@printf "%s\n" "ajout du lien dans la description de la version..."; \
	$(eval pkg_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                           --silent \
	                           "${APIURL}/packages" \
	                      | grep -o -E '\{[^{]*"version":"${VERSION}"[^}]*}' \
	                      | grep -o '"id":[0-9]*' | cut -d: -f 2))
	$(eval file_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                            --silent \
	                            "${APIURL}/packages/${pkg_id}/package_files" \
	                       | grep -o -E '\{[^{]*"file_name":"${ARCHIVE}"[^}]*}' \
	                       | grep -o '"id":[0-9]*' | cut -d: -f 2))
	curl --request POST \
	      --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	      --data name="${ARCHIVE}" \
	      --data url="${REPOSURL}/-/package_files/${file_id}/download" \
	      --data link_type="package" \
	      --output /dev/null --silent \
	      ${APIURL}/releases/${TAGNAME}/assets/links
	@printf "%s\n" "ok"

.PHONY: publish
publish:
	@printf "%s" "mise à jour de la page web..."
	git checkout pages && \
	  ${MAKE} && \
	  git checkout master
	@printf "%s\n" "ok"

.PHONY: check-url
check-url: ${MASTER:.pdf=.tex} ${RNWFILES} ${TEXFILES}
	@printf "%s\n" "vérification des adresses URL dans les fichiers source"
	$(eval url=$(shell grep -E -o -h 'https?:\/\/[^./]+(?:\.[^./]+)+(?:\/[^ ]*)?' $? \
		   | cut -d \} -f 1 \
		   | cut -d ] -f 1 \
		   | cut -d '"' -f 1 \
		   | sort | uniq \
	           | sed -E 's/([()])/\\\1/g'))
	@for u in ${url}; do \
	    printf "%s... " "$$u"; \
	    if curl --output /dev/null --silent --head --fail --max-time 5 $$u; then \
	        printf "%s\n" "ok"; \
	    else \
		printf "%s\n" "invalide ou ne répond pas"; \
	    fi; \
	done

.PHONY: formation-test
formation-test: delete-formation-test sleep create-formation-test

.PHONY: delete-formation-test
delete-formation-test:
	@{ \
	    if curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	            --silent \
	            --url "${BASEAPI}/users/vigou3/projects?simple=yes" | \
	          grep -E -o '"name":"[^"]*"' | \
	          awk 'BEGIN { FS="\"" } { print $$4 }' | \
	          grep -q "^formation-test$$"; \
	    then \
	        printf "%s" "suppression du dépôt formation-test... "; \
	        curl --request DELETE \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --output /dev/null --silent \
	             --url "${BASEAPI}/projects/vigou3%2Fformation-test"; \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: sleep
sleep:
	@{ \
	    printf "%s\n" "... petite pause de 5 sec pour que GitLab fasse le ménage..."; \
	    sleep 5s; \
	}	

.PHONY: create-formation-test
create-formation-test:
	@{ \
	    printf "%s" "création du dépôt formation-test... "; \
	    curl --request POST \
	         --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	         --header "Content-type: application/json" \
	         --data "{ \"name\": \"formation-test\", \"initialize_with_readme\": \"true\", \"default_branch\": \"master\", \"visibility\": \"public\" }" \
	         --output /dev/null --silent \
	         --url "${BASEAPI}/projects/"; \
	    printf "%s\n" "ok"; \
	}

.PHONY: clean
clean:
	${RM} ${MASTER} \
	      ${ARCHIVE} \
	      ${COLLABORATEURS} \
	      *.aux *.log *.snm *.nav *.vrb
